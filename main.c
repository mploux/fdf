/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 06:04:48 by mploux            #+#    #+#             */
/*   Updated: 2016/12/03 00:44:37 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libmlx/mlx.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

int		get_color(int r, int g, int b)
{
	return (r << 16 | g << 8 | b);
}

int		get_brightness(int b)
{
	return (b << 16 | b << 8 | b);
}

int		lerp_color(int a, int b, double n)
{
	t_vec3 ca;
	t_vec3 cb;
	t_vec3 clerp;

	ca.x = (a & 0xff0000) >> 16;
	ca.y = (a & 0xff00) >> 8;
	ca.z = (a & 0xff);
	cb.x = (b & 0xff0000) >> 16;
	cb.y = (b & 0xff00) >> 8;
	cb.z = (b & 0xff);
	clerp.x = ca.x + (cb.x - ca.x) * n;
	clerp.y = ca.y + (cb.y - ca.y) * n;
	clerp.z = ca.z + (cb.z - ca.z) * n;
	return ((int)clerp.x << 16 | (int)clerp.y << 8 | (int)clerp.z);
}

void 	draw_pix(t_fdf *fdf, t_vec2 p, int color)
{
	if (p.x < 0 || p.y < 0 || p.x >= fdf->width || p.y >= fdf->height)
		return ;
	mlx_pixel_put(fdf->mlx, fdf->win, (int)p.x, (int)p.y, color);
}

void 	draw_line(t_fdf *fdf, t_vec2 a, t_vec2 b, int a_color, int b_color)
{
	double	len;
	t_vec2	lerp;
	double	n;
	int		i;

	i = 0;
	n = 0;
	len = sqrt((a.x + b.x) * (a.x + b.x) + (a.y + b.y) * (a.y + b.y));
	while (i < len)
	{
		n = i / len;
		lerp.x = a.x + (b.x - a.x) * n;
		lerp.y = a.y + (b.y - a.y) * n;
		draw_pix(fdf, lerp, lerp_color(a_color, b_color, n));
		i++;
	}
}

void 	fill_map(t_fdf *fdf)
{
	int i;

	while (i < 100)
		fdf->map[i++] = (double)((double)rand() / (double)RAND_MAX) * 100;
}

t_vec2	get_coord(t_fdf *fdf, t_vec3 p)
{
	t_vec2 result;
	double xp;
	double yp;
	double zp;

	xp = p.x;
	yp = (double)(p.y / 100.0) * 0.5;
	zp = p.z;

	// result.x = (int)((xp - 4.5) / zp * fdf->height / 2.0 + fdf->width / 2.0);
	// result.y = (int)((yp - 1.0) / zp * -fdf->height / 2.0 + fdf->height / 2.0);

	xp = (p.x * fdf->rcos + p.z * fdf->rsin);
	zp = (p.z * fdf->rcos - p.x * fdf->rsin);
	result.x = fdf->origin.x + xp * fdf->scale;
	result.y = fdf->origin.y - zp * fdf->scale * fdf->view_angle - p.y * 0.25;
	return (result);
}

void 	draw_map(t_fdf *fdf)
{
	int i;
	t_vec3 ap;
	t_vec3 bp;
	t_vec3 cp;
	t_vec2 a;
	t_vec2 b;
	t_vec2 c;

	i = 0;
	while (i < 99)
	{
		ap.x = i % 10;
		ap.y = fdf->map[i];
		ap.z = i / 10;

		bp.x = (i + 1) % 10;
		bp.y = fdf->map[i + 1];
		bp.z = (i + 1) / 10;

		cp.x = (i + 10) % 10;
		cp.y = fdf->map[i + 10];
		cp.z = (i + 10) / 10;

		a = get_coord(fdf, ap);
		b = get_coord(fdf, bp);
		c = get_coord(fdf, cp);

		draw_pix(fdf, a, 0xffffff);
		if (i % 10 != 9)
			draw_line(fdf, a, b, get_brightness(0), get_brightness(255));
		if (i < 90)
			draw_line(fdf, a, c, get_brightness(0), get_brightness(255));
		i++;
	}
}

int		main()
{
	t_fdf 	fdf;

	fdf.mlx = mlx_init();
	fdf.width = 1280;
	fdf.height = 720;
	fdf.win = mlx_new_window(fdf.mlx, fdf.width, fdf.height, "fdf");
	fdf.origin.x = (fdf.width / 2);
	fdf.origin.y = (fdf.height * 15 / 16);
	fdf.scale = 50;
	fdf.angle = -45 * M_PI / 180;
	fdf.rcos = cos(fdf.angle);
	fdf.rsin = sin(fdf.angle);
	fdf.view_angle = 0.5;

	fill_map(&fdf);
	draw_map(&fdf);
	mlx_loop(fdf.mlx);
	return (0);
}

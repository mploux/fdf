/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 06:28:01 by mploux            #+#    #+#             */
/*   Updated: 2016/11/10 10:38:45 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

typedef struct	s_vec2
{
	int		x;
	int		y;
}				t_vec2;

typedef struct	s_vec3
{
	int		x;
	int		y;
	int		z;
}				t_vec3;

typedef struct	s_fdf
{
	void		*mlx;
	void		*win;
	int			width;
	int			height;
	t_vec2		origin;
	double		scale;
	double		angle;
	double		view_angle;
	double		rcos;
	double		rsin;
	double		map[10 * 10];
}				t_fdf;

#endif

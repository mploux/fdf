/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertex.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 00:15:46 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 19:01:46 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "graphics.h"

t_vertex		vertex(t_data *data, t_vec3 pos, int color)
{
	t_vertex result;

	result.data = data;
	result.pos = vec4(pos.x, pos.y, pos.z, 1);
	result.col = color;
	return (result);
}

static t_vec4	p_div(t_vec4 v)
{
	t_vec4 result;

	result.x = v.x / v.w;
	result.y = v.y / v.w;
	result.z = v.z / v.w;
	result.w = v.w;
	return (result);
}

int				vertex_transform(t_data *data, t_vertex *vert, t_mat4 trs)
{
	t_vec4	pos;
	t_mat4	screen_space;

	screen_space = data->screen_space;
	pos = p_div(mat4_mul_vec4(screen_space, mat4_mul_vec4(trs, vert->pos)));
	pos.x = clamp(pos.x, 0, data->win->w - 1);
	pos.y = clamp(pos.y, 0, data->win->h - 1);
	pos.w = clamp(pos.w, 3, 1000);
	vert->s_pos = pos;
	vert->sc = vec2(pos.x, pos.y);
	return (1);
}

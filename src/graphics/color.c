/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 22:23:22 by mploux            #+#    #+#             */
/*   Updated: 2016/12/06 13:17:46 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "graphics.h"

int		col_lerp(int a, int b, double n)
{
	t_vec3 ca;
	t_vec3 cb;
	t_vec3 cl;

	ca.x = (int)((a & 0xff0000) >> 16);
	ca.y = (int)((a & 0xff00) >> 8);
	ca.z = (int)((a & 0xff));
	cb.x = (int)((b & 0xff0000) >> 16);
	cb.y = (int)((b & 0xff00) >> 8);
	cb.z = (int)((b & 0xff));
	cl.x = LERP(ca.x, cb.x, n);
	cl.y = LERP(ca.y, cb.y, n);
	cl.z = LERP(ca.z, cb.z, n);
	return ((int)cl.x << 16 | (int)cl.y << 8 | (int)cl.z);
}

int		col_diff(int a, int b)
{
	t_vec3 ca;
	t_vec3 cb;
	t_vec3 cl;

	ca.x = (int)((a & 0xff0000) >> 16);
	ca.y = (int)((a & 0xff00) >> 8);
	ca.z = (int)((a & 0xff));
	cb.x = (int)((b & 0xff0000) >> 16);
	cb.y = (int)((b & 0xff00) >> 8);
	cb.z = (int)((b & 0xff));
	cl.x = ca.x - cb.x;
	cl.y = ca.y - cb.y;
	cl.z = ca.z - cb.z;
	return ((int)cl.x << 16 | (int)cl.y << 8 | (int)cl.z);
}

int		color(int r, int g, int b)
{
	return (r << 16 | g << 8 | b);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 15:14:52 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 18:38:44 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "graphics.h"
#include "maths.h"

t_camera	*new_camera(t_data *data, t_vec2 rot, double zoom)
{
	t_camera *cam;

	if (!(cam = (t_camera *)malloc(sizeof(t_camera))))
		return (0);
	cam->proj = mat4_identity();
	cam->data = data;
	cam->z_near = 5;
	cam->z_far = 1000;
	cam->aspect = (double)data->win->w / (double)data->win->h;
	cam->rot = rot;
	cam->scale = vec3(1, 0.1, 1);
	cam->zoom = zoom;
	cam->trs = mat4_identity();
	cam->apos = vec2(0, 0);
	cam->arot = vec2(0, 0);
	cam->ascale = vec2(1, 1);
	cam->azoom = 30;
	return (cam);
}

void		cam_input(t_input *input, t_camera *cam)
{
	if (input->key[KEY_W])
		cam->apos.y--;
	if (input->key[KEY_S])
		cam->apos.y++;
	if (input->key[KEY_A])
		cam->apos.x--;
	if (input->key[KEY_D])
		cam->apos.x++;
	if (input->key[KEY_UP])
		cam->arot.x--;
	if (input->key[KEY_DOWN])
		cam->arot.x++;
	if (input->key[KEY_LEFT])
		cam->arot.y--;
	if (input->key[KEY_RIGHT])
		cam->arot.y++;
	if (input->key[KEY_PG_UP])
		cam->ascale.y += 0.1;
	if (input->key[KEY_PG_DOWN])
		cam->ascale.y -= 0.1;
	if (input->key[KEY_N_ADD])
		cam->azoom--;
	if (input->key[KEY_N_SUB])
		cam->azoom++;
}

void		cam_update(t_camera *cam)
{
	if (cam->ascale.y == 0)
		cam->ascale.y = 0.001;
	cam->pos.x = cam->apos.x * 0.5;
	cam->pos.y = cam->apos.y * 0.5;
	cam->rot.x = cam->arot.x;
	cam->rot.y = cam->arot.y;
	cam->scale.y = cam->ascale.y * 0.5;
	cam->zoom = cam->azoom;
}

t_mat4		cam_transform(t_camera *cam, t_mesh *mesh)
{
	t_mat4	result;
	t_mat4	m[4];
	t_vec3	pos;

	if (cam->data->projection)
		cam->proj = mat4_persp(70.0, cam->aspect, cam->z_near, cam->z_far);
	else
		cam->proj = mat4_ortho(vec2(-cam->zoom * cam->aspect,
									cam->zoom * cam->aspect),
								vec2(-cam->zoom, cam->zoom),
								vec2(-cam->zoom, cam->zoom));
	pos = vec3(-mesh->w / 2 + cam->pos.x,
				-(mesh->max_y * cam->scale.y) / 2,
				-mesh->h / 2 + cam->pos.y);

	m[0] = mat4_rotate_xyz(cam->rot.x, cam->rot.y, 0);
	m[1] = mat4_mul(m[0], mat4_translate(pos));
	m[2] = mat4_mul(mat4_translate(vec3(0, 0, -cam->zoom)), m[1]);
	m[3] = mat4_mul(m[2], mat4_scale(cam->scale));
	result = mat4_mul(cam->proj, m[3]);
	return (result);
}

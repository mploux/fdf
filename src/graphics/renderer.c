/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   renderer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 16:00:00 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 17:47:49 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "graphics.h"
#include "maths.h"

void	draw_pix(t_data *data, double x, double y, int color)
{
	bitmap_draw_pix(data->framebuffer, (int)x, (int)y, color);
}

void	draw_line(t_data *data, t_vec2 a, t_vec2 b, t_vec2 colors)
{
	double	len;
	t_vec2	lerp;
	double	n;
	int		i;
	int		color;

	i = 0;
	n = 0;
	len = sqrt((a.x + b.x) * (a.x + b.x) + (a.y + b.y) * (a.y + b.y));
	while (i < len)
	{
		n = i / len;
		lerp.x = ceil(a.x + (b.x - a.x) * n);
		lerp.y = ceil(a.y + (b.y - a.y) * n);
		color = col_lerp((int)colors.x, (int)colors.y, n);
		draw_pix(data, lerp.x, lerp.y, color);
		i++;
	}
}

void	draw_triangle(t_data *data, t_triangle tri, t_mat4 trs)
{
	double tri_area;

	vertex_transform(data, &tri.top, trs);
	vertex_transform(data, &tri.mid, trs);
	vertex_transform(data, &tri.bot, trs);
	tri_area = (tri.top.s_pos.x - tri.bot.s_pos.x) *
				(tri.mid.s_pos.y - tri.top.s_pos.y) -
				(tri.top.s_pos.x - tri.mid.s_pos.x) *
				(tri.bot.s_pos.y - tri.top.s_pos.y);
	if (tri_area <= 0)
		return ;
	if (tri.top.s_pos.y > tri.mid.s_pos.y)
		ft_swap(&tri.top, &tri.mid, sizeof(t_vertex));
	if (tri.mid.s_pos.y > tri.bot.s_pos.y)
		ft_swap(&tri.mid, &tri.bot, sizeof(t_vertex));
	if (tri.top.s_pos.y > tri.mid.s_pos.y)
		ft_swap(&tri.top, &tri.mid, sizeof(t_vertex));
	scan_triangle(data, tri);
}

double	*new_zbuffer(int width, int height)
{
	double *buffer;

	if (!(buffer = (double *)malloc(sizeof(double) * width * height)))
		error("malloc error !");
	return (buffer);
}

void	clear_zbuffer(t_data *data, double z_far)
{
	int i;

	i = -1;
	while (++i < data->win->w * data->win->h)
		data->zbuffer[i] = z_far;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 14:12:44 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 18:01:19 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "maths.h"
#include "graphics.h"
#include <stdlib.h>

int			create_fdf(t_data *data, const char *name, int width, int height)
{
	t_win		*win;

	if (!(win = (t_win *)malloc(sizeof(t_win))))
		error("malloc error !");
	data->mlx = mlx_init();
	data->input = new_input();
	data->aspect = (double)width / (double)height;
	data->screen_space = mat4_screen_space(width / 2, height / 2);
	data->scan_line = new_scanline(height);
	data->framebuffer = new_bitmap(data, width, height);
	data->zbuffer = new_zbuffer(width, height);
	win->w = width;
	win->h = height;
	win->name = (char *)name;
	win->ctx = mlx_new_window(data->mlx, win->w, win->h, (char *)win->name);
	data->win = win;
	data->filled_mode = 1;
	data->projection = 0;
	data->camera = new_camera(data, vec2(0, 0), 30);
	clear_zbuffer(data, 1000);
	return (1);
}

int			loop(t_data *data)
{
	t_mesh		*mesh;
	t_input		*input;
	t_camera	*cam;
	t_mat4		transformation;

	mesh = ((t_data *)data)->mesh;
	input = ((t_data *)data)->input;
	cam = ((t_data *)data)->camera;
	cam_input(input, cam);
	cam_update(cam);
	transformation = mat4_identity();
	transformation = cam_transform(cam, mesh);

	if (data->filled_mode)
		mesh_draw(mesh, transformation);
	else
		mesh_draw_wireframe(mesh, transformation);
	mlx_put_image_to_window(data->mlx, data->win->ctx,
							data->framebuffer->ctx, 0, 0);
	clear_bitmap(data->framebuffer);
	clear_scanline(data, &data->scan_line);
	clear_zbuffer(data, 1000);
	return (1);
}

void		loop_fdf(t_data *data, t_mesh *mesh)
{
	data->mesh = mesh;
	mlx_hook(data->win->ctx, 2, (1L << 0), &key_hook, data);
	mlx_hook(data->win->ctx, 3, (1L << 1), &key_up_hook, data);
	mlx_loop_hook(data->mlx, &loop, data);
	mlx_loop(data->mlx);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 14:07:59 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 17:25:13 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "graphics.h"
#include <stdlib.h>
#include <fcntl.h>

int		main(int ac, char **av)
{
	t_data	data;

	if (ac != 2)
		error("usage: fdf source_file [color_file]");
	create_fdf(&data, "FdF", 720, 480);
	loop_fdf(&data, load_mesh(&data, av[1]));
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 06:21:14 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 18:44:12 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <unistd.h>
#include <fcntl.h>

static int		read_line_verts(t_data *data, t_mesh *mesh, int y, char *line)
{
	int			x;
	char		**toks;
	t_vertex	vert;

	toks = ft_strsplit(line, ' ');
	x = 0;
	while (toks && toks[x])
	{
		if (!ft_isstrdigit(toks[x]))
			error(toks[x]);
		vert = vertex(data, vec3(x, ft_atoi(toks[x]), y), 0xffffff);
		vert.col = color(
					20 + vert.pos.x * 23,
					20 + clamp(vert.pos.y, 0, 10) * 23,
					20 + vert.pos.z * 23);
		ft_lstadd(&mesh->v_list, ft_lstnew(&vert, sizeof(t_vertex)));
		x++;
	}
	return (x);
}

static int		load_verts(t_data *data, const int fd, t_mesh *mesh)
{
	char		*line;
	t_vec2		pos;
	int			nx;
	int			ret;

	pos.y = 0;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		nx = read_line_verts(data, mesh, pos.y, line);
		if (pos.y != 0 && pos.x != nx)
			error("loader error: map vertices not aligned !");
		pos.x = nx;
		pos.y++;
	}
	if (ret < 0)
		return (error("loader error: invalid file !"));
	mesh->w = pos.x;
	mesh->h = pos.y;
	return (1);
}

t_mesh			*load_mesh(t_data *data, char *file)
{
	int			fd;
	t_mesh		*m;
	int			i;

	fd = open(file, O_RDONLY);
	m = new_mesh(data);
	load_verts(data, fd, m);
	if (!(m->vertices = (t_vertex *)malloc(sizeof(t_vertex) * (m->w * m->h))))
		error("malloc error !");
	i = -1;
	m->max_y = 0;
	while (++i < m->w * m->h)
	{
		m->vertices[i] = *(t_vertex*)(ft_lstget(m->v_list, m->w * m->h - i - 1)->content);
		if (m->vertices[i].pos.y > m->max_y)
			m->max_y = m->vertices[i].pos.y;
	}
	mesh_create_indices(m);
	return (m);
}

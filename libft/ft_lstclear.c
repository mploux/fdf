/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 07:33:27 by mploux            #+#    #+#             */
/*   Updated: 2016/12/09 17:19:11 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void			ft_lstclear(t_list **list)
{
	t_list	*next;

	if (list == NULL || *list == NULL)
		return ;
	while (*list)
	{
		next = (*list)->next;
		free(*list);
		*list = next;
	}
}
